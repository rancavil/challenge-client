# Using image golang
FROM rancavil/golang

# Set the application directory
WORKDIR /app

# update libs and install git
RUN apt-get update -y && apt-get install git -y

# Copy our code from the current folder to /app inside the container
ADD . /app

# Compile the server
RUN go build httpserver.go

# Make port 80 available for links and/or publish
EXPOSE 8000

# Define our command to be run when launching the container
CMD ["./httpserver"]

