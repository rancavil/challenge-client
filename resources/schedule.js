angular.module('720kb', ['720kb.datepicker'])
  .controller('scheduleController', function($scope,$http) {
    var scheduleList = this;
    scheduleList.list = [];

    scheduleList.hours = ['08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'];
    scheduleList.hour = '09:00';

    scheduleList.getAppointments = function() {
        $http.get('http://api.challenge-cloud.cebfca0b.svc.dockerapp.io:8080/schedule/'+scheduleList.date).
                then(function(response) {
                	$scope.message = '';
                	scheduleList.list = response.data;
                	$scope.serverok = true;
        		},function(data){
    				$scope.messageserver = 'Cannot retreive data';
    				$scope.serverok = false;
				});
    };
    scheduleList.addAppointment = function() {
      var starthour = scheduleList.starthour;
      var endhour = scheduleList.endhour;
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      var rehour = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/;

      if(starthour > endhour) {   
        $scope.message = 'The end hour must be great than start hour';
      } else if(starthour == undefined || endhour == undefined) {
        $scope.message = 'You must give me a valid hour range';
      } else if(!rehour.test(starthour) || !rehour.test(starthour)) {
	$scope.message = 'You must give me a valid hour range';
      } else if(!re.test(scheduleList.email)) {
      	$scope.message = 'You must give me a valid email';
      } else {
      	data = {email:scheduleList.email, date:scheduleList.date, starthour:starthour, endhour:endhour};
	console.log(data);
      	$http.post('http://api.challenge-cloud.cebfca0b.svc.dockerapp.io:8080/schedule', data, { headers : {'Content-Type':'application/json'}}).
        	then(function(response) {
             	$scope.message = response.data.message;
             	data.id = response.data.id;
             	if(response.data.status === 'OK')
             		scheduleList.list.push(data);
             		scheduleList.list = sortByKey(scheduleList.list,'date');
             		scheduleList.list = sortByKey(scheduleList.list,'starthour');
             		$scope.serverok = true;
      	},function(data){
    			$scope.messageserver = 'Cannot send data';
    			$scope.serverok = false;
		});

      	scheduleList.email = '';
      	scheduleList.hour = '09:00';
      }
    };

    scheduleList.deleteAppointment = function(id) {
    	console.log('deleting : '+id);
    	$http.delete('http://api.challenge-cloud.cebfca0b.svc.dockerapp.io:8080/schedule/'+id, { headers : {'Content-Type':'application/json'}}).
        	then(function(response) {
            	console.log(response.data.message);
             	$scope.message = response.data.message;  
             	$http.get('http://api.challenge-cloud.cebfca0b.svc.dockerapp.io:8080/schedule/'+scheduleList.date).
                	then(function(response) {
                	scheduleList.list = response.data;
                	if(scheduleList.list.length <=0)
                		$scope.message = '';
                	$scope.serverok = true;
        		},function(data){
    				$scope.messageserver = 'Cannot retreive data';
    				$scope.serverok = false;
				});              	
      	});
    }

    function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

  });
